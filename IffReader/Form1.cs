﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace IffReader
{
    public partial class Form1 : Form
    {
        private byte[] _file;
        private int _filePointer;
        private List<IffType> _iffList = new List<IffType>();

        public Form1()
        {
            InitializeComponent();
            txtFilePath.Text = Properties.Settings.Default.LastPath;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            var openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = "c:\\",
                Filter = @"iff files (*.iff)|*.iff|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtFilePath.Text = openFileDialog1.FileName;

            Properties.Settings.Default.LastPath = openFileDialog1.FileName;
            Properties.Settings.Default.Save();

            ParseFile();
        }

        private void btnParseFile_Click(object sender, EventArgs e)
        {
            ParseFile();
        }

        private void ParseFile()
        {
            dgvIffData.Rows.Clear();
            _filePointer = 0;
            try
            {
                _file = File.ReadAllBytes(txtFilePath.Text);
                btnParseFile.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Error: Could not read file from disk: " + ex.Message);
            }

            Thread.Sleep(2000);

            //OFFSET              Count TYPE   Description                   Bytes
            //0000h                   4 char   ID='FORM'                        16
            //0004h                   1 dword  Size of the whole IFF block       4
            //0008h                   4 char   Type of the IFF file             16

            byte[] headId = ReadFileBytes(4);
            var headIdStr = Encoding.UTF8.GetString(headId, 0, headId.Length);
            Console.WriteLine(@"HEAD Id:"+headIdStr);

            byte[] headSize = ReadFileBytes(4);
            Array.Reverse(headSize);
            var headSizeStr = BitConverter.ToInt32(headSize, 0);
            Console.WriteLine(@"HEAD Size:" + headSizeStr);

            byte[] headType = ReadFileBytes(4);
            var headTypeStr = Encoding.UTF8.GetString(headType, 0, headType.Length);
            Console.WriteLine(@"HEAD Type:" + headTypeStr);

            byte[] headOther = ReadFileBytes(12);
            var headOtherStr = Encoding.UTF8.GetString(headOther, 0, headOther.Length);
            Console.WriteLine(@"HEAD Other:" + headOtherStr);

            ////OFFSET              Count TYPE   Description                        Bytes
            ////0000h                   4 char   ID                                    16
            ////0004h                   1 dword  Blocksize                              4
            ////0008h                   ? byte   Block data, depends on block type.     ?

            int hiddenId = 0;

            while (_filePointer < _file.Length)
            {
                var gridOut = new object[5];

                byte[] chunkId = ReadFileBytes(4);
                string chunkIdStr = Encoding.UTF8.GetString(chunkId, 0, chunkId.Length);
                gridOut[0] = chunkIdStr;

                byte[] chunkSize = ReadFileBytes(4);
                Array.Reverse(chunkSize);
                var chunkSizeStr = BitConverter.ToUInt32(chunkSize, 0);

                int valueSize = 4;

                switch (chunkIdStr)
                {
                    case "BOOL":
                        valueSize = 1;
                        byte[] chunkValueBool = ReadFileBytes(valueSize);

                        if (chunkValueBool[0] == 1)
                            gridOut[1] = "TRUE";
                        if (chunkValueBool[0] == 0)
                            gridOut[1] = "FALSE";
                        _iffList.Add(new IffType { Id = chunkIdStr, ValueIndex = _filePointer - valueSize, ValueSize = valueSize });
                        break;
                    case "FLT ":
                        byte[] chunkValueFlt = ReadFileBytes(valueSize);

                        float chunkValueFltFloat = BitConverter.ToSingle(chunkValueFlt, 0);
                        gridOut[1] = chunkValueFltFloat.ToString(CultureInfo.InvariantCulture);
                        _iffList.Add(new IffType { Id = chunkIdStr, ValueIndex = _filePointer - valueSize, ValueSize = valueSize });
                        break;
                    case "STDS":

                        valueSize = (int)chunkSizeStr;
                        byte[] chunkValueStds = ReadFileBytes(valueSize);
                        var chunkValueStdsStr = Encoding.UTF8.GetString(chunkValueStds, 0, chunkValueStds.Length);
                        var split = chunkValueStdsStr.Split(new[] { "\0" }, StringSplitOptions.None);
                        if (split.Length > 1)
                        {
                            gridOut[1] = split[0];
                            gridOut[2] = split[1];
                            gridOut[3] = split[2];
                        }
                        _iffList.Add(new IffType { Id = chunkIdStr, ValueIndex = _filePointer - valueSize, ValueSize = valueSize });
                        break;
                    case "INT ":
                        byte[] chunkValueInt = ReadFileBytes(valueSize);
                        int chunkValueIntInt = BitConverter.ToInt32(chunkValueInt, 0);
                        gridOut[1] = chunkValueIntInt.ToString(CultureInfo.InvariantCulture);
                        _iffList.Add(new IffType { Id = chunkIdStr, ValueIndex = _filePointer - valueSize, ValueSize = valueSize });
                        break;
                    default:
                        byte[] chunkValueDefault = ReadFileBytes(valueSize);
                        float chunkValueDefaultFloat = BitConverter.ToInt32(chunkValueDefault, 0);
                        gridOut[1] = chunkValueDefaultFloat.ToString(CultureInfo.InvariantCulture);
                        _iffList.Add(new IffType { Id = chunkIdStr, ValueIndex = _filePointer - valueSize, ValueSize = valueSize });
                        break;
                }

                if ((int)chunkSizeStr > valueSize)
                {
                    byte[] chunk1 = ReadFileBytes((int)chunkSizeStr - valueSize);
                    var chunk1Str = Encoding.UTF8.GetString(chunk1, 0, chunk1.Length);
                    var split = chunk1Str.Split(new[] { "\0" }, StringSplitOptions.None);
                    if (split.Length > 1)
                    {
                        gridOut[2] = split[0];
                        gridOut[3] = split[1];
                    }
                }

                gridOut[4] = hiddenId;

                dgvIffData.Rows.Add(gridOut);
                hiddenId ++;
            }
        }

        private byte[] ReadFileBytes(int size)
        {
            var bytes = new byte[size];
            Buffer.BlockCopy(_file, _filePointer, bytes, 0, size);
            _filePointer += size;

            return bytes;
        }

        private void dgvIffData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var rowItem = _iffList[e.RowIndex];

            Console.WriteLine(@"");
            Console.Write(@"Before:");
            for (int i = 0; i < rowItem.ValueSize; i++)
            {
                Console.Write(_file[rowItem.ValueIndex + i]);
            }

            byte[] cellValue;
            switch (rowItem.Id)
            {
                case "INT ":
                    cellValue = BitConverter.GetBytes(int.Parse((string)dgvIffData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value));
                    break;
                case "FLT ":
                    cellValue = BitConverter.GetBytes(float.Parse((string)dgvIffData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value));
                    break;
                case "BOOL":
                    cellValue = BitConverter.GetBytes(bool.Parse((string)dgvIffData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value));
                    break;
                default:
                    return;
            }

            Buffer.BlockCopy(cellValue, 0, _file, rowItem.ValueIndex, rowItem.ValueSize);

            Console.WriteLine(@"");
            Console.Write(@"After:");
            for (int i = 0; i < rowItem.ValueSize; i++)
            {
                Console.Write(_file[rowItem.ValueIndex + i]);
            }

            File.WriteAllBytes(txtFilePath.Text, _file);
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }

    public class IffType
    {
        public string Id { get; set; }
        public int ValueIndex { get; set; }
        public int ValueSize { get; set; }
    }
}
