﻿namespace IffReader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.dgvIffData = new System.Windows.Forms.DataGridView();
            this.btnParseFile = new System.Windows.Forms.Button();
            this.Header = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Identifier1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Identifier2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HiddenId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIffData)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFilePath
            // 
            this.txtFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilePath.Location = new System.Drawing.Point(12, 12);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(770, 20);
            this.txtFilePath.TabIndex = 0;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.Location = new System.Drawing.Point(788, 10);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // dgvIffData
            // 
            this.dgvIffData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvIffData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIffData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Header,
            this.Value,
            this.Identifier1,
            this.Identifier2,
            this.HiddenId});
            this.dgvIffData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvIffData.Location = new System.Drawing.Point(12, 67);
            this.dgvIffData.Name = "dgvIffData";
            this.dgvIffData.Size = new System.Drawing.Size(851, 533);
            this.dgvIffData.TabIndex = 2;
            this.dgvIffData.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvIffData_CellEndEdit);
            // 
            // btnParseFile
            // 
            this.btnParseFile.Location = new System.Drawing.Point(12, 38);
            this.btnParseFile.Name = "btnParseFile";
            this.btnParseFile.Size = new System.Drawing.Size(75, 23);
            this.btnParseFile.TabIndex = 3;
            this.btnParseFile.Text = "Parse";
            this.btnParseFile.UseVisualStyleBackColor = true;
            this.btnParseFile.Click += new System.EventHandler(this.btnParseFile_Click);
            // 
            // Header
            // 
            this.Header.HeaderText = "Header";
            this.Header.Name = "Header";
            this.Header.ReadOnly = true;
            this.Header.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Value
            // 
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            // 
            // Identifier1
            // 
            this.Identifier1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Identifier1.HeaderText = "Identifier1";
            this.Identifier1.Name = "Identifier1";
            this.Identifier1.ReadOnly = true;
            // 
            // Identifier2
            // 
            this.Identifier2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Identifier2.HeaderText = "Identifier2";
            this.Identifier2.Name = "Identifier2";
            this.Identifier2.ReadOnly = true;
            // 
            // HiddenId
            // 
            this.HiddenId.HeaderText = "HiddenId";
            this.HiddenId.Name = "HiddenId";
            this.HiddenId.ReadOnly = true;
            this.HiddenId.Width = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 612);
            this.Controls.Add(this.btnParseFile);
            this.Controls.Add(this.dgvIffData);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtFilePath);
            this.Name = "Form1";
            this.Text = "IffReader";
            ((System.ComponentModel.ISupportInitialize)(this.dgvIffData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.DataGridView dgvIffData;
        private System.Windows.Forms.Button btnParseFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn Header;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn Identifier1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Identifier2;
        private System.Windows.Forms.DataGridViewTextBoxColumn HiddenId;
    }
}

